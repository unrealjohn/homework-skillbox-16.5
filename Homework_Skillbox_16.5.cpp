// Homework_Skillbox_16.5.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
#include <time.h>
#include <stdlib.h>

struct tm buf;
time_t t = time(NULL);

const int N = 20; // size of massive
const int R_MIN = 10; // min value for rand()
const int R_MAX = 49; // max value for rand()

int main()
{
    int SumOfTheOneStringNumbers = 0;

    //Detecting num of The One string
    localtime_s(&buf, &t);
    int NumOfTheOneString = buf.tm_mday % N;

    //Building massive
    const int size = N;
    int array[size][size];
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            bool RowsAndStringsNumFilter = ((i == 0) || (j == 0)); //Filling numbers in massive
            switch (RowsAndStringsNumFilter)
            {
            case true: // random numbers for a 1st string and a 1st row
                array[i][j] = rand() % (R_MAX - R_MIN + 1) + R_MIN;
                break;
            case false: // sum of i and j numbers of 1st string and row for each other strings and rows
                array[i][j] = array[i][0]+ array[0][j];
                break;
            }
            
            //Calculating sum of The One string's numbers
            if (i == NumOfTheOneString)
            {
                SumOfTheOneStringNumbers = SumOfTheOneStringNumbers + array[i][j];
            }
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
    std::cout << "Sum of numbers in string #" << NumOfTheOneString + 1 << " (index #" << NumOfTheOneString << ") is: " << SumOfTheOneStringNumbers << ".\n";
}

